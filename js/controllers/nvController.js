function getID(x) {
  return document.getElementById(x);
}

function showMessageError(idError, message) {
  getID(idError).innerText = message;
}

function layThongTinTuForm() {
  var _tkNV = getID("txtTKNV").value;
  var _hoTenNV = getID("txtHoTenNV").value;
  var _emailNV = getID("txtEmailNV").value;
  var _matKhauNV = getID("txtMatKhauNV").value;
  var _ngayVaoLam = getID("datepicker").value;
  var _luongCoBan = getID("txtLuongCB").value;
  var _chucVu = getID("chucVu").value;
  var _gioLamViec = getID("txtGioLamViec").value;
  var nv = new NhanVien(
    _tkNV,
    _hoTenNV,
    _emailNV,
    _matKhauNV,
    _ngayVaoLam,
    _luongCoBan,
    _chucVu,
    _gioLamViec
  );
  return nv;
}

function renderDSNV(nvArr) {
  var contentHTML = "";
  for (var i = 0; i < nvArr.length; i++) {
    var nv = nvArr[i];
    var contentTr = `<tr>
                  <td>${nv.tkNV}</td>
                  <td>${nv.hoTenNV}</td>
                  <td>${nv.emailNV}</td>
                  <td>${nv.ngayVaoLam}</td>
                  <td>${nv.chucVu}</td>
                  <td>${nv.tinhLuong()}</td>
                  <td>${nv.tinhLoaiNV()}</td>
                  <td><button class="btn btn-danger" onclick="xoaNhanVien('${
                    nv.tkNV
                  }')">Xóa</button></td>
                  <td><button class="btn btn-warning" data-toggle="modal" data-target="#myModal" onclick="suaNhanVien('${
                    nv.tkNV
                  }')">Sửa</button></td>
          </tr>`;
    contentHTML += contentTr;
  }
  getID("tbodyNhanVien").innerHTML = contentHTML;
}

function hienThiThongTinNhanVien(nv) {
  getID("txtTKNV").value = nv.tkNV;
  getID("txtHoTenNV").value = nv.hoTenNV;
  getID("txtEmailNV").value = nv.emailNV;
  getID("txtMatKhauNV").value = nv.matKhauNV;
  getID("datepicker").value = nv.ngayVaoLam;
  getID("txtLuongCB").value = nv.luongCoBan;
  getID("chucVu").value = nv.chucVu;
  getID("txtGioLamViec").value = nv.gioLamViec;
}

function timKiemViTri(idNV, nvArr) {
  var viTri = -1;
  for (var i = 0; i < nvArr.length; i++) {
    var nv = nvArr[i];
    if (nv.tkNV == idNV) {
      viTri = i;
      break;
    }
  }
  return viTri;
}

function resetForm() {
  getID("formQLNV").reset();
}
