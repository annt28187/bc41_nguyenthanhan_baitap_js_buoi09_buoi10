function NhanVien(
  _tkNV,
  _hoTenNV,
  _emailNV,
  _matKhauNV,
  _ngayVaoLam,
  _luongCoBan,
  _chucVu,
  _gioLamViec
) {
  this.tkNV = _tkNV;
  this.hoTenNV = _hoTenNV;
  this.emailNV = _emailNV;
  this.matKhauNV = _matKhauNV;
  this.ngayVaoLam = _ngayVaoLam;
  this.luongCoBan = _luongCoBan;
  this.chucVu = _chucVu;
  this.gioLamViec = _gioLamViec;
  this.tinhLuong = function () {
    var tongLuong = 0;
    var luongCB = this.luongCoBan * 1;
    if (this.chucVu == "Giám đốc") {
      tongLuong = luongCB * 3;
    }
    if (this.chucVu == "Trưởng phòng") {
      tongLuong = luongCB * 2;
    }
    if (this.chucVu == "Nhân viên") {
      tongLuong = luongCB;
    }
    return Intl.NumberFormat("vi-VN", {
      style: "currency",
      currency: "VND",
    }).format(tongLuong);
  };
  this.tinhLoaiNV = function () {
    var xepLoai = "",
      gioLV = this.gioLamViec * 1;
    if (gioLV >= 192) {
      xepLoai = "Xuất sắc";
    } else if (gioLV >= 176) {
      xepLoai = "Giỏi";
    } else if (gioLV >= 160) {
      xepLoai = "Khá";
    } else {
      xepLoai = "Trung bình";
    }
    return xepLoai;
  };
}
