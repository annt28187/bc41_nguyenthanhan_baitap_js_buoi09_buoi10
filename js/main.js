var DSNV = "DSNV_LOCAL";
var dsnv = [];

getLocalStorage();

function setLocalStorage() {
  var dataJSON = JSON.stringify(dsnv);
  localStorage.setItem(DSNV, dataJSON);
  renderDSNV(dsnv);
}

function getLocalStorage() {
  var dataJSON = localStorage.getItem(DSNV);
  if (dataJSON != null) {
    var nvArr = JSON.parse(dataJSON);
    dsnv = nvArr.map(function (item) {
      var nv = new NhanVien(
        item.tkNV,
        item.hoTenNV,
        item.emailNV,
        item.matKhauNV,
        item.ngayVaoLam,
        item.luongCoBan,
        item.chucVu,
        item.gioLamViec
      );
      return nv;
    });
    renderDSNV(dsnv);
  }
}

function themNhanVien() {
  var nv = layThongTinTuForm();
  var isValid = true;
  // Tài khoản NV
  isValid =
    kiemTraRong(nv.tkNV, "tbTKNV", "Tài khoản NV không để trống") &&
    kiemTraTrung(nv.tkNV, dsnv) &&
    kiemTraDoDai(nv.tkNV, "tbTKNV", 4, 6) &&
    kiemTraSo(nv.tkNV, "tbTKNV", "Tài khoản NV phải là số");

  // Họ tên NV
  isValid =
    isValid &
      kiemTraRong(nv.hoTenNV, "tbHoTenNV", "Họ tên NV không để trống") &&
    kiemTraChu(nv.hoTenNV, "tbHoTenNV", "Họ tên NV phải là chữ");
  // Email NV
  isValid =
    isValid & kiemTraRong(nv.emailNV, "tbEmailNV", "Email NV không để trống") &&
    kiemTraEmail(nv.emailNV);
  // Pass NV
  isValid =
    isValid &
      kiemTraRong(nv.matKhauNV, "tbMatKhauNV", "Mật khẩu NV không để trống") &&
    kiemTraMatKhau(nv.matKhauNV);
  // Lương căn bản
  isValid =
    isValid &
      kiemTraRong(nv.luongCoBan, "tbLuongCB", "Lương cơ bản không để trống") &&
    kiemTraSo(nv.luongCoBan, "tbLuongCB", "Lương cơ bản phải là số") &&
    kiemTraSoLuong(nv.luongCoBan, "tbLuongCB", 1000000, 20000000);
  // Chức vụ
  isValid = isValid & kiemTraChucVu();
  // Giờ làm việc
  isValid =
    isValid &
      kiemTraRong(
        nv.gioLamViec,
        "tbGioLamViec",
        "Giờ làm việc không để trống"
      ) &&
    kiemTraSo(nv.gioLamViec, "tbGioLamViec", "Giờ làm việc phải là số") &&
    kiemTraSoLuong(nv.gioLamViec, "tbGioLamViec", 80, 200);
  // Ngày vào làm
  isValid =
    isValid &
      kiemTraRong(
        nv.ngayVaoLam,
        "tbNgayVaoLam",
        "Ngày làm việc không để trống"
      ) && kiemTraNgay();

  if (isValid) {
    dsnv.push(nv);
    setLocalStorage();
    resetForm();
  }
}

function xoaNhanVien(idNV) {
  var result = confirm("Bạn có muốn xóa nhân viên không?");
  if (result) {
    var viTri = timKiemViTri(idNV, dsnv);
    if (viTri != -1) {
      dsnv.splice(viTri, 1);
      renderDSNV(dsnv);
      setLocalStorage();
    }
  }
}

function suaNhanVien(idNV) {
  var viTri = timKiemViTri(idNV, dsnv);
  if (viTri == -1) {
    return;
  }
  var nv = dsnv[viTri];
  hienThiThongTinNhanVien(nv);
  getID("txtTKNV").disabled = true;
}

function capNhatNhanVien() {
  var nv = layThongTinTuForm();
  var viTri = timKiemViTri(nv.tkNV, dsnv);
  if (viTri != -1) {
    var isValid = true;
    isValid =
      kiemTraRong(nv.hoTenNV, "tbHoTenNV", "Họ tên NV không để trống") &&
      kiemTraChu(nv.hoTenNV, "tbHoTenNV", "Họ tên NV phải là chữ");
    // Email NV
    isValid =
      isValid &
        kiemTraRong(nv.emailNV, "tbEmailNV", "Email NV không để trống") &&
      kiemTraEmail(nv.emailNV);
    // Pass NV
    isValid =
      isValid &
        kiemTraRong(
          nv.matKhauNV,
          "tbMatKhauNV",
          "Mật khẩu NV không để trống"
        ) && kiemTraMatKhau(nv.matKhauNV);
    // Lương căn bản
    isValid =
      isValid &
        kiemTraRong(
          nv.luongCoBan,
          "tbLuongCB",
          "Lương cơ bản không để trống"
        ) &&
      kiemTraSo(nv.luongCoBan, "tbLuongCB", "Lương cơ bản phải là số") &&
      kiemTraSoLuong(nv.luongCoBan, "tbLuongCB", 1000000, 20000000);
    // Chức vụ
    isValid = isValid & kiemTraChucVu();
    // Giờ làm việc
    isValid =
      isValid &
        kiemTraRong(
          nv.gioLamViec,
          "tbGioLamViec",
          "Giờ làm việc không để trống"
        ) &&
      kiemTraSo(nv.gioLamViec, "tbGioLamViec", "Giờ làm việc phải là số") &&
      kiemTraSoLuong(nv.gioLamViec, "tbGioLamViec", 80, 200);
    // Ngày vào làm
    isValid =
      isValid &
        kiemTraRong(
          nv.ngayVaoLam,
          "tbNgayVaoLam",
          "Ngày làm việc không để trống"
        ) && kiemTraNgay();
    if (isValid) {
      dsnv[viTri] = nv;
      renderDSNV(dsnv);
      setLocalStorage();
      getID("txtTKNV").disabled = false;
      resetForm();
    }
  }
}

function timKiemLoaiNV() {
  var searchName = getID("searchName").value;
  var loai = searchName.toLowerCase().trim();
  var nvArr = [];

  dsnv.forEach(function (nv) {
    if (nv.tinhLoaiNV().toLowerCase().trim().search(loai) != -1) {
      nvArr.push(nv);
    }
  });
  renderDSNV(nvArr);
}
